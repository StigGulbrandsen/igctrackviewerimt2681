package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	//router
	r := mux.NewRouter()
	//database pointer
	d := &igcDB{}
	//handlers
	r.HandleFunc("/paragliding/", d.rubbishHandler)
	r.HandleFunc("/paragliding/api", d.apiGet)
	r.HandleFunc("/paragliding/api/track", d.trackPostGet)
	r.HandleFunc("/paragliding/api/track/{id}", d.trackGetID)

	//listening to port
	err := http.ListenAndServe(":5050" /*+os.Getenv("PORT")*/, r)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}

}
