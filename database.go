package main

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/gorilla/mux"
	"github.com/marni/goigc"
)

func (db *igcDB) trackLength(track igc.Track) float64 {

	totalDistance := 0.0

	for i := 0; i < len(track.Points)-1; i++ {
		totalDistance += track.Points[i].Distance(track.Points[i+1])
	}

	return totalDistance
}

//handler for redirecting rubbish
func (db *igcDB) rubbishHandler(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "/paragliding/api", http.StatusSeeOther)
}

//handler for GET /api
func (db *igcDB) apiGet(w http.ResponseWriter, r *http.Request) {
	//sets header to json
	//w.Header().Set("Content-Type", "application/json")
	parts := strings.Split(r.URL.Path, "/")

	//var empty = regexp.MustCompile(``)
	var api = regexp.MustCompile(`api`)

	//Handling for /igcinfo/api
	if len(parts) != 3 || !api.MatchString(parts[2]) {
		http.Error(w, "400 - Bad Request, too many url arguments.", http.StatusBadRequest)
		return
	}
	//new json
	var info APIInfo
	info.Duration = timeSince(timeStarted)
	info.Info = "Service for IGC tracks."
	info.Version = "v1"
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(info)
}

//handler for POST and GET /api/track
func (db *igcDB) trackPostGet(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	switch r.Method {
	// GET all ids
	case http.MethodGet:
		ids := make([]string, 0)

		for i := range igcFiles {
			ids = append(ids, igcFiles[i].ID)
		}

		json.NewEncoder(w).Encode(ids)

	case http.MethodPost:

		//Returns id for POST url
		pattern := ".*.igc"

		URL := &_url{}

		var error = json.NewDecoder(r.Body).Decode(URL)
		//if error
		if error != nil {
			fmt.Fprintln(w, "Error!! ", error)
			return
		}
		rand.Seed(time.Now().UnixNano())

		res, err := regexp.MatchString(pattern, URL.URL)
		//if error
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		if res {
			//search for id
			mapID = search(urlMap, URL.URL)
			ID := rand.Intn(1000)

			track, _ := igc.ParseLocation(URL.URL)
			if mapID == -1 {
				//if you find it return it
				if findIndex(urlMap, initialID) {
					uniqueID = ID
					urlMap[uniqueID] = URL.URL
					igcFile := Track{}
					igcFile.ID = strconv.Itoa(ID)
					igcFile.Track = track

					igcFiles = append(igcFiles, igcFile)

					json.NewEncoder(w).Encode(igcFile.ID)
					return
				} else {
					//else return a random
					rand.Seed(time.Now().UnixNano())
					uniqueID = rand.Intn(1000)
					urlMap[uniqueID] = URL.URL
					igcFile := Track{}
					igcFile.ID = strconv.Itoa(ID)
					igcFile.Track = track
					igcFiles = append(igcFiles, igcFile)
					json.NewEncoder(w).Encode(igcFile.ID)
					return

				}
			} else {
				//else if its already positive return it
				uniqueID = search(urlMap, URL.URL)
				json.NewEncoder(w).Encode(uniqueID)
				return

			}
		}
	default:
		//no implementation
		http.Error(w, "No implementation", http.StatusNotImplemented)
		return
	}

}

//handler for GET /api/track/<id>
func (db *igcDB) trackGetID(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	idURL := mux.Vars(r)
	URL := &_url{}

	rNum, _ := regexp.Compile(`[0-9]+`)
	if !rNum.MatchString(idURL["id"]) {
		http.Error(w, "400 - Bad Request", http.StatusBadRequest)
		return
	}

	attributes := &Attributes{}
	//for every igcfile
	for i := range igcFiles {
		//if it's an id
		if igcFiles[i].ID == idURL["id"] {
			attributes.Date = igcFiles[i].Track.Header.Date.String()
			attributes.Pilot = igcFiles[i].Track.Pilot
			attributes.Glider = igcFiles[i].Track.GliderType
			attributes.GliderID = igcFiles[i].Track.GliderID
			attributes.Length = db.trackLength(igcFiles[i].Track)
			attributes.TrackSrcURL = URL.URL
			json.NewEncoder(w).Encode(attributes)
		}

	} //if the id doesn't match
	http.Error(w, "404 ERROR The track with that id doesn't exist", http.StatusNotFound)

}

//handler for GET /api/track/<id>/<field>
func (db *igcDB) trackGetIDField(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	urlFields := mux.Vars(r)

	var rNum, _ = regexp.Compile(`[a-zA-Z_]+`)

	if !rNum.MatchString(urlFields["field"]) {
		http.Error(w, "400", http.StatusBadRequest)
		return
	}

	for i := range igcFiles {

		if igcFiles[i].ID == urlFields["id"] {
			switch {
			case urlFields["field"] == "pilot":
				json.NewEncoder(w).Encode(igcFiles[i].Track.Pilot)
			case urlFields["field"] == "glider":
				json.NewEncoder(w).Encode(igcFiles[i].Track.GliderType)

			case urlFields["field"] == "glider_id":
				json.NewEncoder(w).Encode(igcFiles[i].Track.GliderID)

			case urlFields["field"] == "track_length":
				json.NewEncoder(w).Encode(db.trackLength(igcFiles[i].Track))

			case urlFields["field"] == "h_date":
				json.NewEncoder(w).Encode(igcFiles[i].Track.Header.Date.String())

			case urlFields["field"] == "track_src_url":
				json.NewEncoder(w).Encode(igcFiles[i].Track.Header.Date.String())

			default:
				http.Error(w, "400 - Bad Request, the field you entered is not on our database!", http.StatusBadRequest)
				return
			}

		} else {
			http.Error(w, "400 - Bad Request, the field you entered is not on our database!", http.StatusBadRequest)
			return
		}

	}

}

//handler for GET /api/ticker/latest
func (db *igcDB) apiGetTickerLatest(w http.ResponseWriter, r *http.Request) {

}

//handler for GET /api/ticker/
func (db *igcDB) apiGetTicker(w http.ResponseWriter, r *http.Request) {

}

//handler for GET /api/ticker/<timestamp>
func (db *igcDB) apiGetTickerTimestamp(w http.ResponseWriter, r *http.Request) {

}

//handler for POST /api/webhook/new_track/
func (db *igcDB) apiPostWebTrack(w http.ResponseWriter, r *http.Request) {

}

//handler for GET/DELETE /api/webhook/new_track/<webhook_id>
func (db *igcDB) apiGetDeleteWebTrackID(w http.ResponseWriter, r *http.Request) {

}

//handler for GET /admin/api/tracks_count
func (db *igcDB) adminGetAPITracks(w http.ResponseWriter, r *http.Request) {

}

//handler for DELETE /admin/api/tracks
func (db *igcDB) adminDeleteAPITracks(w http.ResponseWriter, r *http.Request) {

}
