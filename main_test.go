package main

import (
	"net/http"
	"testing"

	"github.com/gorilla/mux"
)

//TestListen tests the port
func TestListen(t *testing.T) {
	r := mux.NewRouter()
	d := &igcDB{}
	//listening to port
	err := http.ListenAndServe(":5050" /*+os.Getenv("PORT")*/, r)
	r.HandleFunc("/paragliding/", d.rubbishHandler)
	if err != nil {
		t.Error("Test failed")
	}

}
