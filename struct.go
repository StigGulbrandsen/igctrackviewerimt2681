//file for all the structs
package main

import (
	"github.com/marni/goigc"
)

//database
type igcDB struct {
	Host                 string
	DatabaseName         string
	TrackCollecltionName string
}

//Attributes for track
type Attributes struct {
	Date        string  `json:"h_date"`
	Pilot       string  `json:"pilot"`
	Glider      string  `json:"glider"`
	GliderID    string  `json:"glider_id"`
	Length      float64 `json:"track_length"`
	TrackSrcURL string  `json:"track_src_url"`
}

//the url
type _url struct {
	URL string `json:"URL"`
}

//Track identified by ID
type Track struct {
	ID    string    `json:"id"`
	Track igc.Track `json:"igc_track"`
}

type APIInfo struct {
	Duration string `json:"uptime"`
	Info     string `json:"info"`
	Version  string `json:"version"`
}

type Ticker struct {
	Latest     string    `json:"t_latest"`
	Start      string    `json:"t_start"`
	Tracks     igc.Track `json:"tracks"`
	Processing string    `json:"processing"`
}

type Webhook struct {
	webhookURL      string `json:"webhookURL"`
	minTriggerValue int32  `json:"minTriggerValue"`
}
